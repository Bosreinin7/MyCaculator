//
//  ViewController.swift
//  Caculator
//
//  Created by Srei Nin on 6/28/18.
//  Copyright © 2018 Srei Nin. All rights reserved.
//  

import UIKit

class ViewController: UIViewController{

    @IBOutlet weak var numTextField: UITextField!
    @IBOutlet weak var resultLbl: UILabel!
    var num1:Double?
    var num2:Double?
    var operatorstr:Character?
    var countSum:Int = 0
    var num:String?
    var resultSum:Double = 0
    var resultMinus:Double = 0
    var resultMuti:Double = 0
    var resultDevide:Double = 0
   
    
    @IBAction func caculateAction(_ sender: Any) {
        self.num = self.numTextField.text
        let NumArr = num?.split(separator: operatorstr!)
        self.num1 = Double(NumArr![0])
        self.num2 = Double(NumArr![1])
        switch operatorstr {
        case "+":self.sum()
          
        case "-":
                 self.resultLbl.text = String(self.num1! - self.num2!)
        case "x": self.resultLbl.text = String(self.num1! * self.num2!)
        default:
         self.resultLbl.text = String(self.num1! / self.num2!)
        }
        
    }
   
    @IBAction func clearAction(_ sender: Any) {
        self.numTextField.text = ""
        self.resultLbl.text    = ""
        self.resultLbl.text    = ""
        self.resultSum         = 0
        self.resultMinus       = 0
        self.resultMuti        = 0
        self.resultDevide      = 0
        
    }
    
    @IBAction func operatorCaculateAction(_ sender: UIButton) {
        self.num = self.numTextField.text
        switch sender.tag {
        case 1: self.operatorstr = "+"
                self.numTextField.text! += "+"
        case 2: self.operatorstr = "-";
                self.numTextField.text! += "-"
        case 3: self.operatorstr = "x";
                self.numTextField.text! += "x"
        default:
                self.operatorstr = "/";
                self.numTextField.text! += "/"
        }
    }
    func sum() {
        let numArrSum = num?.components(separatedBy:"+")
        for n in numArrSum! {
            self.resultSum += Double(n)!
        }
        self.resultLbl.text = String(self.resultSum)
    }
    
}
